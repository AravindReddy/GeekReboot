﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SecurityGatePass
{
	public partial class CreatePass : ContentPage
	{
		public CreatePass()
		{
			InitializeComponent();

            btnCreateGatePass.Clicked += BtnCreateGatePass_Clicked;
		}

        private void BtnCreateGatePass_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new CameraClick());
        }
    }
}
