﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SecurityGatePass
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CameraClick : ContentPage
    {
        public CameraClick()
        {
            InitializeComponent();

            btnClickPicture.Clicked += BtnClickPicture_Clicked;

            btnChooseFromGallery.Clicked += BtnChooseFromGallery_Clicked;
        }

        private async void BtnChooseFromGallery_Clicked(object sender, EventArgs e)
        {
            if (await CheckPermission())
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                    return;
                }
                var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

                });


                if (file == null)
                    return;

                var streamContent = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            }
        }

        private async void BtnClickPicture_Clicked(object sender, EventArgs e)
        {
            if (await CheckPermission())
            {

                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DisplayAlert("No Camera", ":( No camera available.", "OK");
                    return;
                }

                var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    Directory = "SecurityGatePass",
                    Name = "test.png",
                    PhotoSize = PhotoSize.Custom,
                    CustomPhotoSize = 90, //Resize to 90% of original
                    CompressionQuality = 92,
                    AllowCropping = true,
                    DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear
                });

                if (file == null)
                    return;

                await DisplayAlert("File Location", file.Path, "OK");

                var streamContent = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }
        }

        private async Task<bool> CheckPermission()
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

            if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                cameraStatus = results[Permission.Camera];
                storageStatus = results[Permission.Storage];
            }

            if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
            {
                //var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                //{
                //    Directory = "Sample",
                //    Name = "test.jpg"
                //});
                return true;
            }
            else
            {
                await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
                if (Device.RuntimePlatform == Device.iOS)
                {
                    //On iOS you may want to send your user to the settings screen.
                    CrossPermissions.Current.OpenAppSettings();
                }
                return false;
            }
        }
    }
}